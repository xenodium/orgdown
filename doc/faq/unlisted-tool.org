★ [[doc/FAQs.org][← FAQs overview page]] ★

* Why is tool X not listed on the [[doc/Tool-Support.org][tool support page]]?

Most probably because nobody told us about it yet.

The fastest way of getting your tool listed here is to hand in a merge
request that adds the tool to the [[doc/Tool-Support.org][tool support page]] overview and has a
detail page similar to the [[doc/tools/Emacs.org][Emacs page]].

---------------

★ [[doc/FAQs.org][← FAQs overview page]] ★
