★ [[Overview.org][← Overview page]] ★

* Orgdown Parsers

So far, Orgdown does not have dedicated parser libraries to get an
[[https://en.wikipedia.org/wiki/Abstract_syntax_tree][abstract syntax tree]] (AST) from any compatible input file.
[[Contribute.org][This might change in future]].

However, there is a wide range of more than twenty different parsers
for Org-mode syntax in various programming languages and levels of
details. Since Orgdown1 is a limited Org-mode syntax, there is a very
high chance that all of the parsers are able to process Orgdown files.

Fortunately, there exist a few web pages that collect and list
Org-mode syntax parsers. Therefore, we only link to those collections:

- [[https://orgmode.org/worg/org-tools/][https://orgmode.org/worg/org-tools/]]
- [[https://alphapapa.github.io/org-almanac/#Parsing][https://alphapapa.github.io/org-almanac/#Parsing]]

One specific parser should be mentioned here, since it is the parser
used to render Org-mode files for GitHub and GitLab:
[[https://github.com/wallyqs/org-ruby][https://github.com/wallyqs/org-ruby]]

