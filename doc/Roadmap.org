★ [[Overview.org][← Overview page]] ★

* Orgdown Roadmap

Orgdown was published with its first level, Orgdown1, in November 2021
at [[https://emacsconf.org/2021/][EmacsConf]]. This was the just the first step of a potential success
story for Orgdown syntax. It was necessary to have something "less"
than Org-mode, and it was important to emphasize the difference between
the Org-mode syntax and the Org-mode Elisp implementation.

So, what about the future outlook for Orgdown?

If we assume that Orgdown1 is widely adapted by people working with
lightweight markup documents, the next logical step are *more [[Orgdown-Levels.org][Orgdown
levels]]*. With each new level, more and more Org-mode syntax elements
join the universe of Orgdown.

The [[Contribute.org][contribution page]] lists further ideas on how Orgdown can help
people: a formal specification for all Orgdown levels, an Orgdown
Language Server Protocol (LSP), and probably the vision with the
biggest potential impact: a [[https://en.wikipedia.org/wiki/Gemini_(protocol)][Gemini]] web of Orgdown documents.

*There is plenty of room for Orgdown in this digital world!*
